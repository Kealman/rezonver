/**
 * Created by Evgeniy on 30.03.14.
 */
require.config({
    baseUrl: "js",
    paths: {
        jquery: 'lib/jquery-1.11.0',
        underscore: 'lib/underscore',
        backbone: 'lib/backbone',
        templates: '../templates',
        text: "lib/text"
    },
    shim: {
        backbone: {
            deps: ['underscore', 'jquery'],
            exports: 'Backbone'
        },
        'underscore': {
            exports: '_'
        },
        jquery: {
            exports: "$"
        }
    }

});

require([
    "app"
], function(App){
    window.firstRender = true;
    new App();
});
