/**
 * Created by Evgeniy on 11.05.14.
 */

define([
    "backbone",
    "classes/Page",
    "pageConfig"
], function(Backbone, Page, Conf){

    var Home = Page.extend({

        el: "#main-container",

        render: function(){
            this.priority = Conf.get("home", "main").priority;

            return Conf.get("home", "main").template;
        }



    });

    return new Home();

});