/**
 * Created by Evgeniy on 11.05.14.
 */
define([
    "backbone",
    "classes/Page"
], function(Backbone, Page){

    return Page.extend({

        setTemplate: function(template, val){
            this.template =  _.template(template, val);

            return this;
        },

        render: function(){
            return this.template;
        }

    });

});
