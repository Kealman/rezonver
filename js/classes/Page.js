/**
 * Created by Evgeniy on 27.05.14.
 */
define([
    "backbone",
    "underscore"
], function(Backbone, _){

    return Backbone.View.extend({

        container: "#main-container",

        wrapper: function(html){
            var cl = "secondPage";
            if(window.firstRender) cl = "mainPage";

            return "<li class='page "+cl+"'>"+html+"</li>";
        },

        left: function(render, callback){
            $(this.container).append(this.wrapper(render.call(this)));
            this.animate("left", callback);
        },

        right: function(render, callback){
            $(this.container).prepend(this.wrapper(render.call(this)));
            $(this.container).css("left", "-100%");
            this.animate("right", callback);
        },

        animate: function(type, callback){
              var percent = 0
                  , _then = this;

              if(type == "left") percent = -100;

              $(this.container).animate({
                  left: percent+"%"
              }, function(){
                  $(".mainPage").remove();
                  $(".secondPage").addClass("mainPage").removeClass("secondPage");
                  $(_then.container).css("left", 0);
                  window.currentPage = _then;
                  callback();
              });
        },

        setPriority: function(p){
            this.priority = p;
        },

        firstRender: function(){
            $(this.container).html(this.wrapper(this.render()));
            window.firstRender = false;
            window.currentPage = this;
        },

        show: function(callback){
           if(!_.isFunction(callback)) callback = function(){};

           if(window.firstRender) return this.firstRender.call(this);

           if(this.priority < window.currentPage.priority)
                this.right.call(this, this.render, callback);
            else
                this.left.call(this, this.render, callback);
        }

    })

});
