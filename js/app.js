/**
 * Created by Evgeniy on 11.05.14.
 */
define([
    "backbone",
    "controllers/home",
    "controllers/page",
    "pageConfig",
    "text!templates/hybrid-7.html",
    "text!templates/pride-7.html"
], function(Backbone, Home, Page, pageConfig, hb, pr){

    var App = Backbone.Router.extend({

        routes: {
            "": "home",
            "!/prime/:page" : "prime",
            "!/hybrid/:page": "hybrid"
        },

        initialize: function(){
            Backbone.history.start();
        },

        home: function(){
            Home.show();
        },

        prime: function(p){
            this.showPage("prime", p);
        },

        hybrid: function(p){
            this.showPage("hybrid", p);
        },

        showPage: function(type, p){
            var page = new Page();

            var conf = pageConfig.get(type, p);

            page.setPriority(conf.priority);
            page.setTemplate(conf.template, {content: "Page "+conf.priority});
            page.show();
        }

    });

    var sendEmail = function(type){
        var email = $("input[type=email]").val()
            , phone = $("input[type=phone]").val()
            , stop =true;

        if(!email) {
            $("input[type=email]").css("background", "#FFD2D2");
            stop = false;
        }

        if(!phone){
            $("input[type=phone]").css("background", "#FFD2D2");
            stop = false;
        }

        if(!stop) return;

        $.post("/mail.php", {email: email, phone: phone},function(err, data){

            var html = type === "pride" ? pr : hb;
            $("#main-container li").html(html);

        })
    }

    $("body").keyup(function(e){
        if(e.keyCode == 39) pageConfig.next(window.currentPage.priority)
        if(e.keyCode == 37) pageConfig.back(window.currentPage.priority)
    })

    $("body").click(function(e){
        if(e.target.attr && (e.target.attr('name') || e.target.attr('name'))) return;
        if(window.currentPage.priority >= 100) pageConfig.next(window.currentPage.priority)
        if(window.currentPage.priority <= 80) pageConfig.back(window.currentPage.priority)

    });

    $("#main-container").on("click", "input", function(e){
        e.preventDefault();
        console.log(12344);
        sendEmail("pride")
    })


    return App;

});
