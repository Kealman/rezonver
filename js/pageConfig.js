/**
 * Created by Evgeniy on 28.05.14.
 */
define([
    "text!templates/page.html",
    "text!templates/home.html",
    "text!templates/hybrid-1.html",
    "text!templates/hybrid-2.html",
    "text!templates/hybrid-3.html",
    "text!templates/hybrid-4.html",
    "text!templates/hybrid-5.html",
    "text!templates/hybrid-6.html",
    "text!templates/hybrid-7.html",
    "text!templates/hybrid-8.html",
    "text!templates/pride-1.html",
    "text!templates/pride-2.html",
    "text!templates/pride-3.html",
    "text!templates/pride-4.html",
    "text!templates/pride-5.html",
    "text!templates/pride-6.html",
    "text!templates/pride-7.html",
    "text!templates/pride-8.html",
    "text!templates/contacts.html"
], function(Page, Home, Hybrid1, Hybrid2, Hybrid3,Hybrid4, Hybrid5, Hybrid6, Hybrid7, Hybrid8,  Pride1, Pride2, Pride3,Pride4,Pride5, Pride6,Pride7,Pride8, Contacts){
    //Это говно должно идти строго по порядку от самого левого к самому правому (просто лень придумывать нормальный алгоритмы)
    var list = {
        "prime": {
            "page8": {
                priority: 10,
                template: Pride2
            },
            "page7": {
                priority: 20,
                template: Pride8
            },
            "page6": {
                priority: 30,
                template: Pride6
            },
            "page5": {
                priority: 40,
                template: Pride5
            },
            "page4": {
                priority: 50,
                template: Pride4
            },
            "page3": {
                priority: 60,
                template: Pride3
            },
            "page2": {
                priority: 70,
                template: Pride2
            },
            "page1": {
                priority: 80,
                template: Pride1
            }
        },

        "home":{
            "main": {
                priority: 90,
                template: Home
            }
        },

        "hybrid": {
            "page1": {
                priority: 100,
                template: Hybrid1
            },

            "page2":{
                priority: 110,
                template: Hybrid2
            },

            "page3":{
                priority: 120,
                template: Hybrid3
            },

            "page4":{
                priority: 130,
                template: Hybrid4
            },

            "page5":{
                priority: 140,
                template: Hybrid5
            },

            "page6":{
                priority: 150,
                template: Hybrid6
            },

            "page8":{
                priority: 160,
                template: Hybrid8
            },

            "contacts": {
                priority: 170,
                template: Contacts
            }
        }


    }

    var Conf = function(){}
    Conf.prototype.get = function(type, page){
        try{
            return list[type][page];
        }catch(e){
            return "";
        }
    }


    Conf.prototype.next = function(p){
        var prev = 0;
        if(p === 160) return window.location.hash = "#!/hybrid/contacts";
        if(p === 170) return window.location.hash = "#";
        for (var type in list){
            for(var page in list[type]){
                if(prev <= p && p < list[type][page].priority) {
                    var route = "!/"+type+"/"+page;

                    if(route == "!/home/main") route = "";

                    return window.location.hash = route;
                }
                prev = list[type][page].priority;
            }
        }
    }

    Conf.prototype.back = function(p){
        var prev = 0
            , obj;

        if(p === 20) return window.location.hash = "#!/hybrid/contacts";
        if(p === 170) return window.location.hash = "#";

        for (var type in list){
            for(var page in list[type]){
                if(prev < p && p <= list[type][page].priority) {

                    var route = "!/"+obj.type+"/"+obj.page;

                    if(route == "!/home/main") route = "";

                    return window.location.hash = route;
                }
                prev = list[type][page].priority;
                obj = {
                    type: type,
                    page: page
                }
            }
        }
    }

    return new Conf();


});
